pytest-testinfra
molecule>=3.2.2
molecule-openstack
openstacksdk
git+https://github.com/dingus9/ansible-coverage-callback.git@master#egg=ansible_coverage_callback
